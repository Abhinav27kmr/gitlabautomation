import sys
from ConfigReader import ConfigReader as cr
from Connection import Connection
#sys.path.append("/builds/Abhinav27kmr/GitlabAutomation/TestCases/")
sys.path.append('.\TestCases')
from GitlabLogin import GitlabLogin as gll
from GitlabPullClone import GitlabPullClone as glc
from GitlabPush import GitlabPush as glp

class Run:
    def execute_test(self):
        try: 
            souce = Connection()
            gl1 = souce.get_connection()
            gl1.auth()
            print("Gitlab Global Authenticated")
        except Exception as e:
            print("Error Check Connection.py & ConfigReader.py")
        else:
            newInstance = cr.instance()
            gll.instance(newInstance.ssh_url, newInstance.gitlab_url, newInstance.ssh_urlc) #Debug Done
            glc.instance( newInstance.project_id, newInstance.username, newInstance.gitlab_token, newInstance.ssh_url, newInstance.https_url, newInstance.branch, newInstance.home_path, newInstance.home_path_https, gl1 ) #debug Done
            glp.instance(newInstance.ssh_url, newInstance.https_url, newInstance.branch,newInstance.home_path, newInstance.home_path_https, gl1)

if __name__ == '__main__':
    run = Run()
    run.execute_test()
        