import configparser

class ConfigReader: 
    configFileName = 'config/config.ini'
    _instance = None

    def __init__(self):
        pass

    @classmethod
    def instance(cls):
        if cls._instance is None:
            newInstance = cls.__new__(cls)
            newInstance.config()
            cls._instance = newInstance
        return cls._instance

    def config(self):
        configFile = configparser.ConfigParser()
        configFile.read(self.configFileName)
        print('Reading Configuration from Config.ini File')
        self.configFile = configFile
        self.gitlab_url = self.readvalues('GitlabServer', 'url')
        self.username = self.readvalues('GitlabServer', 'username')
        self.gitlab_token = self.readvalues('GitlabServer', 'token')
        self.ssh_url = self.readvalues('ProjectUrl', 'ssh_url')
        self.ssh_urlc = self.readvalues('SSH_Connect_Data', 'ssh_urlc')
        self.https_url = self.readvalues('ProjectUrl', 'https_url')
        self.branch = self.readvalues('ProjectUrl', 'branch')
        self.expected=self.readvalues('SSH_Connect_Data', 'expected')
        self.project_id = self.readvalues( 'ProjectUrl', 'project_id' )
        self.home_path = self.readvalues('ClonedPath', 'home_path')
        self.home_path_https = self.readvalues('ClonedPath', 'home_path_https')

    def readvalues(self, section, field):
        value = None
        try:
            value = self.configFile[section][field]
        except:
            print("Issue reading value " + section + " - " + field)
        return value
