from ConfigReader import ConfigReader as cr
import gitlab
import requests
import os

class Connection:
    _instance = None

    def __init__(self):
        pass

    @classmethod
    def instance(cls):
        newInstance = cls.__new__(cls)
        newInstance.get_connection()
        cls._instance = newInstance
        return cls._instance

    def get_connection(self):
        self._newInstance = cr.instance()
        self.url = self._newInstance.gitlab_url
        self.token = self._newInstance.gitlab_token
        return gitlab.Gitlab(self.url,private_token=self.token)