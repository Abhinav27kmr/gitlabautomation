About the Project: 
 - Automation of Gitlab Functionalities with help of Python
 - Test Cases include: 
   1. Git Clone/Push
   2. Git Login
   3. Schedule_pipeline

Pre-Requisite: [in Order to Run this Project in your Local]
  - Modules
  - Python3.6/3.6+
  
Explanation of the Project Modules: 
- Project Driver File: MainPython.py 
- Divided Test Cases into Modules and imported with in the Driver File
- Have Made Config.ini File for all the Required Authentication, Project Name, ID and ManyMore
- Have Used Connection.py to Make Authentication with Gitlab

Used Modules: 
 - Python-Gitlab
 - Sys
 - configparser
 - os
 - requests
 

Will be Updating More in Near Future


