import subprocess as subprocess
import os, shutil
from ConfigReader import ConfigReader as cr


class GitlabPullClone:

    def __init__(self):
        pass

    @classmethod
    def instance(cls,project_id, username, gitlab_token, ssh_url, https_url1, branch, home_path, home_path_https, gl1):
        newInstance = cls.__new__( cls )
        newInstance.git_clone_ssh(project_id, ssh_url, branch, home_path,gl1) #Debug Done
        newInstance.git_clone_https(project_id, username, gitlab_token, https_url1, branch, home_path_https, gl1) #Debug Done
        newInstance.git_pull( ssh_url, branch, home_path, home_path_https ) #Debug Done
        newInstance.counts_rosetta2(project_id, gl1, branch, home_path, ssh_url) #Debug Done
        cls._instance = newInstance
        return cls._instance

    def subprocess_cmd(self, command):
        process = subprocess.Popen( command, stdout=subprocess.PIPE, shell=True )
        proc_stdout = process.communicate()[0].strip()
        return proc_stdout

     
#********************************MEthod1****************************************************
    def git_clone_ssh(self, project_id, ssh_url, branch, home_path, gl1):
        try:
            folder_path = ssh_url.split( '/' )[-1].split( '.' )[-2]
            print( '#################### Testing Git clone through ssh #######################''\n' )
            details = gl1.projects.get( project_id)  # clone through ssh
            url = details.ssh_url_to_repo
            os.chdir(home_path)
            if os.path.isdir(os.path.join(home_path,folder_path)):
                print( 'Repo directory already exists , deleting it ....' )
                os.system( 'rmdir /S /Q "{}"'.format( os.path.join( home_path, folder_path ) ) )
            print( "Directory path where we want to clone " + home_path )
            cmd = 'git clone -b ' + branch + ' ' + url
            print( cmd )
            out = self.subprocess_cmd( cmd )
            os.chdir( os.path.join( home_path, folder_path ) )
        except Exception as e:
            print( 'issue occured in ssh git clone' )
            print( e )       
    
#*******************************Method 2**********************************************    
    def git_clone_https(self,project_id,username,token, https_url1, branch, home_path_https, gl1):
        try:
            folder_path = https_url1.split( '/' )[-1].split( '.' )[-2]
            print( '#################### Tesing git clone through https #######################''\n' )
            details = gl1.projects.get( project_id)
            url = details.http_url_to_repo  # clone through https
            url2 = ('//' + username + ':' + token + '@').join( url.split( '//' ) )
            print(url2)
            os.chdir( home_path_https )
            if os.path.isdir( os.path.join( home_path_https, folder_path ) ):
                print( 'Repo directory already exists , deleting it ....' )
                os.system( 'rmdir /S /Q "{}"'.format( os.path.join( home_path_https, folder_path ) ) )
            print( "Directory path where we want to clone " + home_path_https )
            cmd = 'git clone -b ' + branch + ' ' + url2
            print('git clone -b ' + branch + ' ' + url)
            # print(cmd)
            out = self.subprocess_cmd( cmd )
            os.chdir( os.path.join( home_path_https, folder_path ) )
        except Exception as e:
            print( 'issue occured in https git clone' )
            print( e )


#*******************************Method 3**********************************************    
    def git_pull(self, ssh_url,branch, home_path,home_path_https): # git pull testing
        try:
            print( '#################### Testing Git Pull ssh #######################''\n' )
            folder_path = ssh_url.split( '/' )[-1].split( '.' )[-2]
            os.chdir( os.path.join( home_path, folder_path ) )
            repo_path = os.path.join( home_path, folder_path )
            # cmd1 = "git remote set  origin" + ssh_url
            # out1 = self.subprocess_cmd(cmd1)
            cmd = "git pull origin " + branch
            print( "Repo path  " + repo_path )
            print( cmd )
            out = self.subprocess_cmd( cmd )
            print( out.decode( "utf-8" ) )
            print( '#################### Testing Git Pull https #######################''\n' )
            os.chdir( os.path.join( home_path_https, folder_path ) )
            repo_path = os.path.join( home_path_https, folder_path )
            cmd = "git pull origin " + branch
            print( "Repo path  " + repo_path )
            print( cmd )
            out = self.subprocess_cmd( cmd )
            print( out.decode( "utf-8" ) )
        except Exception as e:
            print( 'issue occured in  git pull' )
            print( e )   


#*******************************Method 4**********************************************    
    def counts_rosetta2(self, project_id, gl1, branch, home_path, ssh_url):  # remove the branch parameter
        print( "***** Started Validation *****" )
        project = gl1.projects.get( project_id)  # remove the hard coded pass it from the cmd
        branch = project.branches.get( branch )
        total_file_count = 0
        folder_path = ssh_url.split( '/' )[-1].split( '.' )[-2]
        os.chdir(os.path.join( home_path, folder_path))
        files = project.repository_tree( all=True, recursive=True, ref='main', lazy=True )
        for j in files:
            if j['type'] == 'blob':
                total_file_count += 1

        remote_count = total_file_count

        local_count = self.subprocess_cmd( 'git ls-files | find /c /v ""' )  # check different branches
        local_count = int( local_count.decode( "utf-8" ) )
        print( "\tNumber of files in local repo", local_count )
        print( "\tNumber of files in remo repo", remote_count )

        if remote_count == local_count:
            print( "\tFiles are matching in local and Remote Repo" )
            print( "\tValidation Passed" )
        else:
            print( "\tValidation Failed" )