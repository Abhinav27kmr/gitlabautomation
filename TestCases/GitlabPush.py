import git
import subprocess as subprocess
import os,shutil
from datetime import datetime
from ConfigReader import ConfigReader as cr

class GitlabPush:


    def __init__(self):
        pass

    @classmethod
    def instance(cls, ssh_url, https_url, branch,home_path, home_path_https, gl1):
        newInstance = cls.__new__(cls)
        repo = newInstance.git_push_ssh(ssh_url,branch, home_path, home_path_https)
        #newInstance.git_config(repo)
        newInstance.git_push_https(repo,https_url,branch)
        #newInstance.counts_rosetta2(gl1,branch)

        cls._instance = newInstance
        return cls._instance


#***************************METHOD 1**************************************
# Git push using ssh url of the repo
    def git_push_ssh(self, ssh_url, branch,home_path, home_path_https):
        print('#################### Testing git push using SSH #######################''\n')
        folder_path = ssh_url.split( '/' )[-1].split( '.' )[-2]
        os.chdir( os.path.join( home_path_https, folder_path ) )
        
        dirname = os.path.join(
            os.getcwd(),
            datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

        if os.path.isdir(dirname):
            shutil.rmtree(dir())
        
        os.mkdir(dirname)
        repo = git.Repo.init(dirname)
        repo.git.checkout('-b',branch)
        print(os.getcwd())
        dirname2 = 'SSH'
        os.chdir(dirname)
        os.mkdir(dirname2)

        with open(os.path.join(dirname2, "README.md"), "w+") as fp:
            fp.write("This file is updated by python script using gitlab ssh   " + '**' + str(datetime.now()) + '**')
            fp.close()
        repo.git.add('--all')
        repo.git.commit('-m','commit message from python script for ssh')

        try:
            print(f"\tBranch: {branch}")
            print(f"\tCommand: git remote add origin {ssh_url}")
            self.origin = repo.create_remote('origin', ssh_url)
            print(f"\tCommand: git push -u origin {branch}")
            self.origin.push(branch)
            print("\trepo push succesfully using ssh url")

        except Exception as e:
            print('issue occured in ssh git push')
            print(e)
        return repo    

#***************************METHOD 2**************************************
    def git_config(self, repo):
        print('#################### Initiated git config #######################''\n')
        self._newInstance= cr.instance()
        with repo.config_writer() as git_config:
            print("\tCommand: git config user.email")
            git_config.set_value('user','email','iamkumarabhinav27@gmail.com')
            print("\tCommand: git config user.name")
            git_config.set_value('user', 'name', 'Abhinav27kmr')
        # To check configuration values, use `config_reader()`
        # with repo.config_reader() as git_config:
        #     print(git_config.get_value('user', 'email'))
        #     print(git_config.get_value('user', 'name'))
        print('\tfinished git config')

#***************************METHOD 3**************************************
    def counts_rosetta2(self, gl1, branch): # remove the branch parameter
        print('#################### Started Validation #######################''\n')
        project = gl1.projects.get(27597) # remove the hard coded pass it from the cmd
        branch = project.branches.get(branch)
        total_file_count = 0

        files = project.repository_tree(all=True, recursive=True, ref='master', lazy=True)
        for j in files:
            if j['type'] == 'blob':
                total_file_count += 1


        remote_count = total_file_count

        local_count = self.subprocess_cmd('git ls-files | find /c /v ""') # check different branches
        local_count = int(local_count.decode("utf-8"))
        print("\tNumber of files in local repo", local_count)
        print("\tNumber of files in remo repo", remote_count)

        if remote_count == local_count:
            print("\tFiles are matching in local and Remote Repo")
            print("\tValidation Passed")
        else:
            print("\tValidation Failed")

#***************************METHOD4**************************************
    def git_push_https(self, repo, https_url, branch):
        print('#################### Testing git push using https #######################''\n')
        self.dirname = os.path.join(
            os.getcwd(),
            datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))

        if os.path.isdir(self.dirname):
            shutil.rmtree(dir())

        # repo = git.Repo.init(dirname)
        # repo.git.checkout('-b',branch)
        # print(os.getcwd())
        dirname2 = 'HTTPS'
        os.mkdir(dirname2)


        with open(os.path.join(dirname2, "README.md"), "w+") as fp:
            fp.write("This file is created by python script using https  " + '**' + str(datetime.now()) + '**')
            fp.close()

        repo.git.add('--all')
        repo.git.commit('-m','commit message from python script using https')

        try:
            username = 'Abhinav27kmr'
            token = 'glpat-B5uqX1b996EW3Eydh7kN'
           # https_url = f'https://{username}:{token}@gitlab.rosetta.ericssondevops.com/Abhinav27kmr/dockerproject27.git'
            https_url = r'https://gitlab.com/Abhinav27kmr/dockerproject27.git'

            print(f"\tBranch: {branch}")
            print(f"\tCommand: git remote add origin2 {https_url}")
            origin = repo.create_remote('origin2', https_url)
            print(f"\tCommand: git push -u origin {branch}")
            origin.push(branch)
            print("\trepo push succesfully using https url")

        except Exception as e:
            print('issue occured in https git push')
            print(e)
