import gitlab
import subprocess as subprocess
from subprocess import Popen, PIPE
import os
import requests
from ConfigReader import ConfigReader as cr


class GitlabLogin:
    file = open("Logs.txt", "w")

    def __init__(self):
        pass

    @classmethod
    def instance(cls, ssh_url, https_url, ssh_urlc):
        newInstance = cls.__new__(cls)
        newInstance.ssh_connect(ssh_url, ssh_urlc) #Debug Done
        newInstance.https_connect(https_url) #Debug Done
        newInstance.https_wrong_token(https_url) #Debug Done


        cls._instance = newInstance
        return cls._instance



    def subprocess_cmd(self, command):
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        stdout = process.communicate()[0]
        stderr = process.communicate()[1]
        return stdout, stderr


#****************************Method1**********************************
# Git Login using ssh
    def ssh_connect(self, ssh_url, ssh_urlc):
        try:
            print('#################### TEST CASE - SSH Connect #######################''\n')
            self.file.write('####################TEST CASE - SSH Connect#######################''\n')
            cmd = "\"C://Program Files//Git//usr//bin//ssh\"" + ' -T ' + ssh_urlc
            actual, error = self.subprocess_cmd(cmd)
            # process = Popen(cmd, stdout=PIPE, stderr=None, shell=True)
            # output = process.communicate()[0]
            # print(output)
            actual_string = str(actual)
            print(actual_string)
            error_string = str(error)
            if cr.instance().readvalues('SSH_Connect_Data','expected') in actual_string:
                self.file.write("Output: " + actual_string + "\n")
                print("Test Case SSH Connect: PASS")
                self.file.write("Test Case SSH Connect: PASS\n")
                self.file.write("\n")
            else:
                self.file.write("Error: " + error_string + "\n")
                print("Test Case 2: FAIL")
                self.file.write("Test Case SSH Connect: FAIL\n")
                self.file.write("\n")
        except Exception as e:
            print("Test Case SSH Connect: FAIL")
            self.file.write(str(e) + "\n")
            self.file.write("Test Case SSH Connect: FAIL\n")


#****************************Method2********************************** 
# HTTPS Login check
    def https_connect(self, https_url):
        print('#################### TEST CASE - HTTPS Connect #######################''\n')
        self.file.write('####################TEST CASE - HTTPS Connect#######################''\n')
        gl = gitlab.Gitlab(https_url, private_token=cr.instance().readvalues('GitlabServer','token'))
        try:
            gl.auth()
            actual = gl.users.get(cr.instance().readvalues('HTTPS_Connect_Data','UserID')).name
            self.file.write("Output: " + actual + "\n")
            expected = "KUMAR ABHINAV"
            if expected == actual:
                print("Test Case HTTPS Connect: PASS")
                self.file.write("Test Case HTTPS Connect: PASS\n")
                self.file.write("\n")
            else:
                print("Test Case HTTPS Connect: FAIL")
                self.file.write("Test Case HTTPS Connect: FAIL\n")
                self.file.write("\n")
        except Exception as e:
            print("Test Case HTTPS Connect: FAIL")
            self.file.write("Error: " + str(e) + "\n")
            self.file.write("Test Case HTTPS Connect: FAIL\n")
            self.file.write("\n")

#****************************Method3**********************************
# HTTPS wrong token check
    def https_wrong_token(self, https_url):
        print('#################### TEST CASE - HTTPS Wrong token #######################''\n')
        self.file.write('####################TEST CASE - HTTPS Wrong token#######################''\n')
        gl = gitlab.Gitlab(https_url, private_token=cr.instance().readvalues('GitlabServer', 'wrong_token'))
        try:
            gl.auth()
            print("Test case HTTPS Wrong Access Token: FAIL")
            self.file.write("Error: Authenticated Successfully\n")
            self.file.write("Test Case HTTPS Wrong Access Token: FAIL\n")
            self.file.write("\n")
        except Exception as e:
            actual = str(e)
            self.file.write("Output: " + actual + "\n")
            expected = "401: 401 Unauthorized"
            if actual == expected:
                print("Test Case HTTPS Wrong Access Token: Pass")
                self.file.write("Test Case HTTPS Wrong Access Token: PASS\n")
                self.file.write("\n")
            else:
                print("Test case HTTPS Wrong Access Token: FAIL")
                self.file.write("Test Case HTTPS Wrong Access Token: FAIL\n")
                self.file.write("\n")

